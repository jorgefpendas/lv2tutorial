#!/bin/bash

g++ -fvisibility=hidden -fPIC -Wl,-Bstatic -Wl,-Bdynamic -Wl,--as-needed -shared -pthread `pkg-config --cflags lv2` -lm `pkg-config --libs lv2` MyTestTone.cpp -o MyTestTone.so

cp *.ttl *.so ~/.lv2/MyTestTone.lv2
