/* include libs */
#include <cstddef>
#include <cstdint>
#include <cstdlib>
#include <cmath>
#include "lv2.h"

/* class definition */
class MyTestTone 
{
  private:
    float* audio_out_ptr;
    float* wave_ptr;
    float* freq_ptr;
    float* level_ptr;
    double rate;
    double position;

  public:
    MyTestTone (const double sample_rate);
    // ~MyTestTone (); // if not defined, C++ knows how to delete it
    void connectPort (const uint32_t port, void* data_location);
    void activate ();
    void run (const uint32_t sample_count);
    // void deactivate (); // deleted because there is nothing to do
};

MyTestTone::MyTestTone (const double sample_rate):
  audio_out_ptr (nullptr),
  wave_ptr (nullptr),
  freq_ptr (nullptr),
  level_ptr (nullptr),
  rate (sample_rate),
  position (0.0)
{

}

void MyTestTone::connectPort (const uint32_t port, void* data_location) {
  switch (port)
  {
    case 0:
      audio_out_ptr = (float*) data_location;
      break;
    case 1:
      wave_ptr = (float*) data_location;
      break;
    case 2:
      freq_ptr = (float*) data_location;
      break;
    case 3:
      level_ptr = (float*) data_location;
      break;
    default:
      break;
  }
}

void MyTestTone::activate () {
  position = 0.0;
}

void MyTestTone::run (const uint32_t sample_count) {
  if ((!audio_out_ptr) || (!freq_ptr) || (!level_ptr)) return;

  for (uint32_t i = 0; i < sample_count; ++i) {
    if (*wave_ptr < 1.0) audio_out_ptr[i] = sin (2.0 * M_PI * position) * *level_ptr;
    if ((*wave_ptr >= 1) && (*wave_ptr < 2.0)) audio_out_ptr[i] = round(1 + sin (2.0 * M_PI * position)) * *level_ptr;
    if (*wave_ptr >= 2) audio_out_ptr[i] = position * *level_ptr;
    position += *freq_ptr / rate;
    if (position > 1) position -= 1;
  }
}

/* internal core methods */
//static LV2_Handle instantiate (const struct LV2_Descriptor *descriptor, double sample_rate, const char *bundle_path, const LV2_Feature *const *features) {
static LV2_Handle instantiate (const LV2_Descriptor *descriptor, double sample_rate, const char *bundle_path, const LV2_Feature *const *features) {
  MyTestTone* m = new MyTestTone (sample_rate);
  return m;
}

static void connect_port (LV2_Handle instance, uint32_t port, void *data_location) {
  MyTestTone* m = (MyTestTone*) instance;
  if (m) m->connectPort (port, data_location);
}

static void activate (LV2_Handle instance) {
  MyTestTone* m = (MyTestTone*) instance;
  if (m) m->activate ();
}

static void run (LV2_Handle instance, uint32_t sample_count) {
  MyTestTone* m = (MyTestTone*) instance;
  if (m) m->run (sample_count);
}

static void deactivate (LV2_Handle instance) {
  /* not needed here */
}

static void cleanup (LV2_Handle instance) {
  MyTestTone* m = (MyTestTone*) instance;
  if (m) delete m;
}

static const void* extension_data (const char *uri) {
  return NULL;
}

/* descriptor */
static LV2_Descriptor const descriptor = {
  "https://gitlab.com/jorgefpendas/lv2tutorial/-/tree/main/MyTestTone",
  instantiate,
  connect_port,
  activate /* or NULL */,
  run,
  deactivate /* or NULL */,
  cleanup,
  extension_data /* or NULL */
};

/* interface */
extern "C" LV2_SYMBOL_EXPORT const LV2_Descriptor* lv2_descriptor(uint32_t index) {
  if (index == 0) return &descriptor;
  else return NULL;
}
